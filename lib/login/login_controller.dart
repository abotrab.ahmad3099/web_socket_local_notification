import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginController with ChangeNotifier {
  final Dio _dio = Dio();
  final String baseUrl = 'http://85.215.42.190:8000';
  String? _accessToken;
  bool _isLoggedIn = false;

  String? get accessToken => _accessToken;
  bool get isLoggedIn => _isLoggedIn;

  Future<int> signIn(String username, String password,
      void Function(String?) updateToken) async {
    try {
      _isLoggedIn = true;
      notifyListeners(); // Notify listeners when sign-in process starts

      final response = await _dio.post(
        '$baseUrl/api/login/',
        data: {'username': username, 'password': password},
      );

      // Check if the response is successful
      if (response.statusCode == 200) {
       
        final accessToken = response.headers.map['set-cookie']
            ?.firstWhere((cookie) => cookie.startsWith('access_token='))
            .split(';')
            .first
            .substring('access_token='.length);

        // Store the access token in SharedPreferences
        final prefs = await SharedPreferences.getInstance();
        await prefs.setString('access_token', accessToken ?? "");

        _accessToken = accessToken;
        _isLoggedIn = false;
        updateToken(accessToken);

        notifyListeners();
        return response.statusCode ?? -1 ;
      } else {
        throw Exception('Failed to sign in');
      }
    } catch (error) {
      throw Exception('Failed to sign in: $error');
    }
  }
}
