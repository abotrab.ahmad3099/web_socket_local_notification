import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../web_socket_notification/message_notify_providor.dart';
import 'login_controller.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver {
  TextEditingController email = TextEditingController();
  late Sink<dynamic> myMessageSink;
  TextEditingController password = TextEditingController();
  String? token;
  late SharedPreferences sharedPreferences;

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    final isBackground = state == AppLifecycleState.paused;
    if (isBackground) {
      context.read<MessageNotifierProvider>().notifyStream.listen((value) {
        Map<String, dynamic> message = jsonDecode(value);
        context.read<MessageNotifierProvider>().addInbox(message);
      });
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  shared() async {
    sharedPreferences = await SharedPreferences.getInstance();
     email.text = sharedPreferences.getString("userName") ?? "";
    password.text = sharedPreferences.getString("password") ?? "";
    setState(() {
      
    });
  }

  @override
  void initState()  {
    shared();
   
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: const Text("Flutter Web Socket"),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Login",
                style: TextStyle(
                  fontFamily: 'Montserrat',
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  height: 29 / 24,
                  // line-height in Flutter is defined as height
                  letterSpacing: 0.03,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 20),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Text(
                  "Stay signed in with your account to keep your contacts up to date",
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    height: 22 / 14,

                    // line-height in Flutter is defined as height
                    letterSpacing: 0.02,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(height: 30),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: TextField(
                  controller: email,
                  style: TextStyle(
                      color: Theme.of(context).textTheme.bodyMedium!.color),
                  decoration: InputDecoration(
                    hintText: "Enter your user name",
                    hintStyle: TextStyle(
                      fontFamily: 'Nunito Sans',
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      height: 14 / 14,

                      color: Theme.of(context).textTheme.bodyMedium!.color,
                      letterSpacing: 0.02,
                      // background color of the hint text
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(color: Colors.white),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(color: Colors.white),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(color: Colors.white),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(color: Colors.white),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: TextField(
                  controller: password,
                  style: TextStyle(
                      color: Theme.of(context).textTheme.bodyMedium!.color),
                  decoration: InputDecoration(
                    hintText: "Enter your Password",
                    hintStyle: TextStyle(
                      fontFamily: 'Nunito Sans',
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      height: 14 / 14,

                      color: Theme.of(context).textTheme.bodyMedium!.color,
                      letterSpacing: 0.02,
                      // background color of the hint text
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(color: Colors.white),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(color: Colors.white),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(color: Colors.white),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: const BorderSide(color: Colors.white),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20),
              Consumer<LoginController>(
                builder: (context, controller, child) {
                  if (controller.isLoggedIn) {
                    return const CircularProgressIndicator(
                      color: Colors.blue,
                    );
                  } else {
                    return Container(
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      height: 58,
                      width: 285,
                      child: MaterialButton(
                        onPressed: () async {
                          // Store the context before entering the asynchronous code block
                          final scaffoldContext = context;
                          SharedPreferences sharedPreferences =
                              await SharedPreferences.getInstance();
                          try {
                            int statusCode = await controller
                                .signIn(email.text, password.text, (token) {
                              // Use the stored context only if it's still valid
                              if (scaffoldContext.mounted) {
                                sharedPreferences.setString(
                                    "userName", email.text);
                                sharedPreferences.setString(
                                    "password", password.text);
                                context
                                    .read<MessageNotifierProvider>()
                                    .setToken(token!);
                                ScaffoldMessenger.of(scaffoldContext)
                                    .showSnackBar(
                                  const SnackBar(
                                      content:
                                          Text('make connection with server')),
                                );
                              }
                            });

                            if (statusCode != 200) {
                              if (scaffoldContext.mounted) {
                                ScaffoldMessenger.of(scaffoldContext)
                                    .showSnackBar(
                                  const SnackBar(content: Text('Failed Login')),
                                );
                              }
                            }
                          } catch (error) {
                            // Handle sign-in error
                          }
                        },
                        child: const Text(
                          "Sign In",
                          style: TextStyle(
                            fontFamily: 'Nunito Sans',
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            letterSpacing: 0.03,
                            color: Colors.white,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                  }
                },
              ),
              const SizedBox(height: 20),
              Consumer<LoginController>(
                builder: (context, controller, child) {
                  return Container(
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    height: 58,
                    width: 285,
                    child: MaterialButton(
                      onPressed: () async {
                        // Store the context before entering the asynchronous code block
                        SharedPreferences sharedPreferences =
                            await SharedPreferences.getInstance();
                        token =
                            sharedPreferences.getString("access_token") ?? "";
                        final scaffoldContext = context;
                        context
                            .read<MessageNotifierProvider>()
                            .setToken(token!);
                        ScaffoldMessenger.of(scaffoldContext).showSnackBar(
                          const SnackBar(
                              content: Text('make connection with server')),
                        );
                      },
                      child: const Text(
                        "Connect with server",
                        style: TextStyle(
                          fontFamily: 'Nunito Sans',
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 0.03,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
