import 'dart:convert';
import 'package:flutter/foundation.dart' show ChangeNotifier;
import 'package:rxdart/rxdart.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_local_notification/web_socket_notification/notification_model.dart';
import 'package:web_socket_local_notification/web_socket_notification/notification_service.dart';

class MessageNotifierProvider with ChangeNotifier {
  MessageNotifierProvider();

  List<dynamic> inbox = [];

  late IOWebSocketChannel _channel;
  late BehaviorSubject<dynamic> _notifyStream;

  BehaviorSubject<dynamic> get notifyStream => _notifyStream;

  Sink<dynamic> get notifyMessageSink => _channel.sink;

  void addInbox(dynamic message) {
    inbox.add(message);
    notifyListeners();
  }

  // Method to set the token
  void setToken(String token) {
    _channel = IOWebSocketChannel.connect(
      Uri.parse("ws://85.215.42.190:8000/ws/notifications/"),
      headers: {"token": token},
    );
    _notifyStream = BehaviorSubject()..addStream(_channel.stream);

    _notifyStream.listen((value) {
      var response = jsonDecode(value);
      NotificationModel notificationModel =
          NotificationModel.fromJson(response["Notifications"]);
      NotificationService service = NotificationService();
      service.showNotification(
          title: notificationModel.title, body: notificationModel.body);
    });
  }
}
