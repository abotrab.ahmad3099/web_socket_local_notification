import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '/web_socket_notification/message_notify_providor.dart';
import 'login/login_controller.dart';
import 'login/login_page.dart';
import 'web_socket_notification/notification_service.dart';

void main() async {
  runApp(const MyApp());
  WidgetsFlutterBinding.ensureInitialized();
  await NotificationService().initNotification();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => MessageNotifierProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => LoginController(),
        ),
      ],
      builder: (context, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Websocket',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const HomePage(),
        );
      },
    );
  }
}
